<?php

namespace App;

use illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['nama'];
    protected $primarykey = 'id';
    protected $keyType = 'string';
    public $incerementing = false;

    protected static function boot()
    {

        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
